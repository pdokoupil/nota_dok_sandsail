function getInfo()
    return {
        tooltip = "Move with a position clipping",
        onNoUnits = SUCCESS,
        parameterDefs = {
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

function Clip(x, lower_bound, upper_bound)
    if x < lower_bound then
        return lower_bound
    elseif x > upper_bound then
        return upper_bound
    end
    return x
end

function ClipVectorPosition(position, mapWidth, mapHeight)
    position.x = Clip(position.x, 1, mapWidth)
    position.z = Clip(position.z, 1, mapHeight)
    return position
end

alreadyMoving = false
function Run(self, units, parameter)

    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ
    local position = ClipVectorPosition(parameter.position, mapWidth, mapHeight)

    if self.lastPositions == nil then
        self.lastPositions = {}
    end

    if not alreadyMoving then
        for i = 1, #units do	
            SpringGiveOrderToUnit(units[i], CMD.MOVE, position:AsSpringVector(), {})
        end
        alreadyMoving = true
        return RUNNING
    end

    -- Comment this to avoid waiting for (almost) reaching the position
    for i = 1, #units do	
        local pointX, pointY, pointZ = SpringGetUnitPosition(units[i])
        local currUidLoc = Vec3(pointX, pointY, pointZ)
        position["y"] = pointY
        
        local lastPos = self.lastPositions[i]
        
        local distanceToTarget = currUidLoc:Distance(position)

        if  lastPos ~= nil then
            local distanceMovedSinceLastTime = lastPos:Distance(currUidLoc)
            -- If we moved just by a little and we still have some distance to go, we return a failure (stucked)
            if distanceMovedSinceLastTime <= 1 and distanceToTarget > 10 then
                return FAILURE
            end
        end

        self.lastPositions[i] = currUidLoc

        -- We still have to go
        if distanceToTarget > 10 then
            return RUNNING
        end
    end

    return SUCCESS
end

function Reset(self)
    alreadyMoving = false
    self.lastPositions = {}
    return self
end